﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Status {

	[System.Serializable]
	public class Status {
		static int MIN_DURATE = 0;
		static int MAX_DURATE = 9999;

		public StatusTypeEnum type;
		public bool permanentFlag = false;
		[SerializeField] protected int _durate;

		public int durate {
			get {
				return Mathf.Clamp(_durate, MIN_DURATE, MAX_DURATE);
			}
			set {
				_durate = Mathf.Clamp(value, MIN_DURATE, MAX_DURATE);
			}
		}



		public Status(StatusTypeEnum newType, int newDurate, bool permanent = false) {
			this.type = newType;
			this.durate = newDurate;
			this.permanentFlag = permanent;
		}

		public bool IsExpired() {
			if (durate != 0 || permanentFlag) {
				return false;
			}

			return true;
		}

	}


}