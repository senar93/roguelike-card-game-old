﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Status {

	[System.Serializable]
	public class StatusCombatContainer {
		public bool enableEventLog = true;
		/// <summary>
		/// blocca l'esecuzione di alcune funzioni
		/// usata negli eventi Try per fermare il calcolo
		/// </summary>
		protected bool stopCalculate = false;

		#region Event
		[SerializeField] protected string eventNameTryAddStatus;
		[SerializeField] protected string eventNameAddStatus;
		[SerializeField] protected string eventNameTryRemoveStatus;
		[SerializeField] protected string eventNameRemoveStatus;
		[SerializeField] protected string eventNameTryExpireStatus;
		[SerializeField] protected string eventNameExpireStatus;
		#endregion

		[SerializeField] protected List<Status> statusList;



		/// <summary>
		/// restituisce lo status richiesto, se non è presente restituisce null
		/// </summary>
		/// <param name="type">stato da cercare</param>
		/// <returns>oggetto Status del tipo corrispondente</returns>
		public Status GetStatus(StatusTypeEnum type) {
			return statusList.Find(x => x.type == type);
		}

		/// <summary>
		/// aggiunge, o aggiorna uno status
		/// </summary>
		/// <param name="type">stato da aggiungere</param>
		/// <param name="durate">durata dello stato</param>
		/// <param name="permanent">è permanente?</param>
		public void AddStatus(StatusTypeEnum type, int durate, GameObject eventListner, bool permanent = false) {
			EventSystem.SendEvent(eventNameTryAddStatus, eventListner, new Status(type, durate, permanent));
			if (!stopCalculate) {
				Status tmpStatus = GetStatus(type);
				if (tmpStatus == null) {
					AddStatusToList(type, durate, permanent);
				}
				else {
					tmpStatus.durate += durate;
					if (!tmpStatus.permanentFlag) {
						tmpStatus.permanentFlag = permanent;
					}
				}
				EventSystem.SendEvent(eventNameAddStatus, eventListner, GetStatus(type));
			}
			else {
				stopCalculate = false;
			}
		}

		/// <summary>
		/// rimuove lo stato selezionato dalla lista
		/// </summary>
		/// <param name="type">stato da rimuovere</param>
		public void RemoveStatus(StatusTypeEnum type, GameObject eventListner) {
			Status tmpStatus = GetStatus(type);
			EventSystem.SendEvent(eventNameTryRemoveStatus, eventListner, tmpStatus);
			if (!stopCalculate) {
				statusList.Remove(tmpStatus);
				EventSystem.SendEvent(eventNameRemoveStatus, eventListner, tmpStatus);
			} else {
				stopCalculate = false;
			}
		}

		/// <summary>
		/// rimuove gli elementi scaduti dalla lista
		/// </summary>
		public void Update(GameObject eventListner) {
			for (int i = 0; i < statusList.Count; i++) {
				if (statusList[i].IsExpired()) {
					Status tmpStatus = statusList[i];
					EventSystem.SendEvent(eventNameTryExpireStatus, eventListner, tmpStatus);
					if (!stopCalculate) {
						statusList.RemoveAt(i);
						i--;
						EventSystem.SendEvent(eventNameExpireStatus, eventListner, tmpStatus);
					}
					else {
						stopCalculate = false;
					}
				}
			}
		}

		public void StopCalculate() {
			stopCalculate = true;
		}



		private void AddStatusToList(StatusTypeEnum type, int durate, bool permanent = false) {
			statusList.Add(new Status(type, durate, permanent));
		}


	}


}