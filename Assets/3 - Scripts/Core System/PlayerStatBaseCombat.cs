﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stat.Dungeon;
using Stat.Combat;
using Status;
using PlayerClass;

public class PlayerStatBaseCombat : MonoBehaviour {
	[SerializeField] protected PlayerStatBaseDungeon playerStatDungeon;

	public HealthCombat health;
	public GoldCombat gold;
	public LuckCombat luck;

	public StrengthCombat strength;
	public SpellPowerCombat spellPower;
	public EnduranceCombat endurance;

	public StatusCombatContainer statusList;


	// Use this for initialization
	void Start() {
		if (!playerStatDungeon) {
			playerStatDungeon = this.GetComponent<PlayerStatBaseDungeon>();
		}

		//DEBUG
		//InitStat();
		/*statusList.AddStatus(StatusTypeEnum.a, 3);
		statusList.AddStatus(StatusTypeEnum.a, 3);
		statusList.AddStatus(StatusTypeEnum.b, 0);
		statusList.AddStatus(StatusTypeEnum.c, 0, true);
		statusList.AddStatus(StatusTypeEnum.c, 1);
		statusList.Update();
		*/

	}


	/// <summary>
	/// setta i parametri delle statistiche in combattimento in base a quelli del dungeon
	/// </summary>
	public void InitStat() {
		InitHealth();
		InitGold();
		InitLuck();
	}

	/// <summary>
	/// setta i parametri degli hp in combattimento in base a quelli del dungeon
	/// </summary>
	private void InitHealth() {
		HealthDungeon hpDungeon = playerStatDungeon.healthDungeon;
		health.ChangeMaxHealth(hpDungeon.GetMaxValue(), null);
		health.GetMinValue().value = hpDungeon.GetMinValue().value;
		health.GetMinValue().enable = hpDungeon.GetMinValue().enable;
		health.deathThreshold = hpDungeon.deathThreshold;
		health.HealDamage(hpDungeon.GetValue(), null);
	}

	private void InitGold() {
		GoldDungeon goldDungeon = playerStatDungeon.gold;
		gold.GetMinValue().value = goldDungeon.GetMinValue().value;
		gold.GetMinValue().enable = goldDungeon.GetMinValue().enable;
		gold.GetMaxValue().value = goldDungeon.GetMaxValue().value;
		gold.GetMaxValue().enable = goldDungeon.GetMaxValue().enable;
		gold.ChangeValue(goldDungeon.GetValue(), null);
	}

	private void InitLuck() {
		LuckDungeon luckDungeon = playerStatDungeon.luck;
		luck.GetMinValue().value = luckDungeon.GetMinValue().value;
		luck.GetMinValue().enable = luckDungeon.GetMinValue().enable;
		luck.GetMaxValue().value = luckDungeon.GetMaxValue().value;
		luck.GetMaxValue().enable = luckDungeon.GetMaxValue().enable;
		luck.ChangeValue(luckDungeon.GetValue(), null);
	}



}