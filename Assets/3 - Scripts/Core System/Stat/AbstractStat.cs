﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stat {

	[System.Serializable]
	abstract public class AbstractStat {

		public static float DEFAULT_LIMIT_VALUE = 99999;

		[SerializeField] private float _value;
		[SerializeField] protected ValueWithFlag minValue = new ValueWithFlag(0, false);
		[SerializeField] protected ValueWithFlag maxValue = new ValueWithFlag(0, false);

		/// <summary>
		/// valore della statistica
		/// </summary>
		protected float value {
			get {
				return _value;
			}
			set {
				if (minValue.enable) {
					tmpMinValue = minValue.value;
				}
				else {
					tmpMinValue = -DEFAULT_LIMIT_VALUE;
				}
				if (maxValue.enable) {
					tmpMaxValue = maxValue.value;
				}
				else {
					tmpMaxValue = DEFAULT_LIMIT_VALUE;
				}

				_value = Mathf.Clamp(value, tmpMinValue, tmpMaxValue);
			}
		}



		#region ONLY FOR INTERNAL USE

		private float tmpMaxValue, tmpMinValue;

		#endregion


		#region Get and Set function

		public float GetValue() {
			return value;
		}
		public ValueWithFlag GetMaxValue() {
			return maxValue;
		}
		public ValueWithFlag GetMinValue() {
			return minValue;
		}

		#endregion

		protected float Add(float incValue) {
			float tmpOldValue = value;
			value += incValue;
			return value - tmpOldValue;
		}
		protected float Sub(float subValue) {
			float tmpOldValue = value;
			value -= subValue;
			return value - tmpOldValue;
		}

	}


}