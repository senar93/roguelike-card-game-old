﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stat.Dungeon {

	[System.Serializable]
	public class HealthDungeon : AbstractStatWithEvent {

		#region Event
		[SerializeField] protected string eventNameTryMaxHpChangeDungeon = "Event_";
		[SerializeField] protected string eventNameMaxHpChangeDungeon = "Event_";
		[SerializeField] protected string eventNameTryPlayerDeathDungeon = "Event_";
		[SerializeField] protected string eventNamePlayerDeathDungeon = "Event_";

		#endregion

		public float deathThreshold = 0;



		public float DealDamage(float damage, GameObject eventListner) {
			float tmpDamage = base.ChangeValue(-damage, eventListner);
			if (IsDead()) {
				EventSystem.SendEvent(eventNameTryPlayerDeathDungeon, eventListner, 1);
				if(!stopCalculate) {
					EventSystem.SendEvent(eventNamePlayerDeathDungeon, eventListner, 1);
				} else {
					stopCalculate = false;
				}
			}
			return tmpDamage;
		}

		public float HealDamage(float heal, GameObject eventListner) {
			float tmpHeal = base.ChangeValue(heal, eventListner);
			return tmpHeal;
		}

		public float ChangeMaxHealth(float changeValue, GameObject eventListner) {
			EventSystem.SendEvent(eventNameTryMaxHpChangeDungeon, eventListner, changeValue);
			
			if (!stopCalculate) {
				float tmpOldMaxHp = maxValue.value;
				float trueChangedValue = changeValue;

				if (changeValue != 0) {
					maxValue.value += changeValue;
					maxValue.value = Mathf.Clamp(maxValue.value, minValue.value + 1, AbstractStat.DEFAULT_LIMIT_VALUE);
					value = Mathf.Clamp(value, minValue.value, maxValue.value);
					trueChangedValue = maxValue.value - tmpOldMaxHp;
					if (trueChangedValue != 0) {
						EventSystem.SendEvent(eventNameMaxHpChangeDungeon, eventListner, trueChangedValue);
					}
				}

				return trueChangedValue;
			} else {
				stopCalculate = false;
				return 0;
			}

		}
		public float ChangeMaxHealth(ValueWithFlag changeValue, GameObject eventListner) {
			maxValue.enable = changeValue.enable;
			return ChangeMaxHealth(changeValue.value, eventListner);
		}

		public bool IsDead() {
			if (value <= deathThreshold) {
				return true;
			}
			else {
				return false;
			}
		}


	}


}