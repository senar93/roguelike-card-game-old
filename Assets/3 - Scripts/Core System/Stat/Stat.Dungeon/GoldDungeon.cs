﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stat.Dungeon {

	[System.Serializable]
	public class GoldDungeon : AbstractStatWithEvent {

		/// <summary>
		/// incrementa / decrementa il valore della statistica e lancia l'evento corrispondente
		/// </summary>
		/// <param name="changeValue">incremento / decremento richiesto</param>
		/// <param name="eventListner">gameObject che riceverà l'evento</param>
		/// <returns>incremento / decremento reale</returns>
		public new float ChangeValue(float changeValue, GameObject eventListner) {
			return base.ChangeValue(changeValue, eventListner);
		}

	}


}