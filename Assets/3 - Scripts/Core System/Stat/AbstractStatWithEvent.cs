﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stat {

	[System.Serializable]
	abstract public class AbstractStatWithEvent : AbstractStat {

		[SerializeField] protected bool enableEventLog = true;
		/// <summary>
		/// blocca l'esecuzione di alcune funzioni
		/// usata negli eventi Try per fermare il calcolo
		/// </summary>
		protected bool stopCalculate = false;

		#region Event
		[SerializeField] protected string eventNameTryChangeValue;
		[SerializeField] protected string eventNameChangeValue;
		#endregion


		public void StopCalculate() {
			stopCalculate = true;
		}

		/// <summary>
		/// PROTECTED | incrementa / decrementa il valore della statistica e lancia l'evento corrispondente
		/// </summary>
		/// <param name="changeValue">incremento / decremento richiesto</param>
		/// <param name="eventListner">gameObject che riceverà l'evento</param>
		/// <returns>incremento / decremento reale</returns>
		protected float ChangeValue(float changeValue, GameObject eventListner) {
			float tmpOldStat = this.GetValue();
			float trueChangedValue = 0;

			EventSystem.SendEvent(eventNameTryChangeValue, eventListner, changeValue);
			if (!stopCalculate) {
				if (changeValue > 0) {
					this.Add(changeValue);
				}
				else if (changeValue < 0) {
					this.Sub(-changeValue);
				}

				trueChangedValue = this.GetValue() - tmpOldStat;

				if (trueChangedValue != 0) {
					EventSystem.SendEvent(eventNameChangeValue, eventListner, trueChangedValue);
				}
			} else {
				stopCalculate = false;
			}
			return trueChangedValue;
		}

	}


}