﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stat {

	/// <summary>
	/// variabile con un flag che indica se il valore va considerato o meno
	/// </summary>
	[System.Serializable]
	public class ValueWithFlag {

		/// <summary>
		/// indica se il valore contenuto in value va considerato o meno
		/// </summary>
		[SerializeField] private bool _haveValue;
		/// <summary>
		/// valore della variabile
		/// </summary>
		[SerializeField] private float _value;


		/// <summary>
		/// indica se il valore contenuto in value va considerato o meno
		/// </summary>
		public bool enable {
			get {
				return _haveValue;
			}
			set {
				_haveValue = value;
			}
		}

		/// <summary>
		/// valore della variabile
		/// </summary>
		public float value {
			get {
				return _value;
			}
			set {
				_value = value;
			}
		}

		public ValueWithFlag(float newValue, bool enable) {
			value = newValue;
			this.enable = enable;
		}

	}


}