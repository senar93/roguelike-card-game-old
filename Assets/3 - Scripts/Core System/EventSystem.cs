﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventSystem {
	public static bool enableEventLog = true;

	public static void SendEvent(string eventName, GameObject eventListner, object parameter = null) {
		if (eventListner) {
			if (enableEventLog) {
				Debug.Log(eventName + " : " + parameter);
			}
			eventListner.BroadcastMessage(eventName, parameter, SendMessageOptions.DontRequireReceiver);
		}
	}

}
