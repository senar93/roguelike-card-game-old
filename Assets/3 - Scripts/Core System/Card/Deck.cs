﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Card {

	[System.Serializable]
	public class Deck {

		protected bool stopCalculate = false;

		#region Event
		[SerializeField] protected string eventNameTryShuffle;
		[SerializeField] protected string eventNameShuffle;
		[SerializeField] protected string eventNameTryAddCard;
		[SerializeField] protected string eventNameAddCard;
		[SerializeField] protected string eventNameTryRemoveCard;
		[SerializeField] protected string eventNameRemoveCard;
		#endregion

		[SerializeField] protected List<AbstractCard> cardList;




		public void StopCalculate() {
			stopCalculate = true;
		}

		public void AddCard(AbstractCard card, GameObject eventListner) {
			EventSystem.SendEvent(eventNameTryAddCard, eventListner);
			if (!stopCalculate) {
				

				EventSystem.SendEvent(eventNameAddCard, eventListner);
			}
			else {
				stopCalculate = false;
			}
		}

		public void RemoveCard(int index, GameObject eventListner) {
			EventSystem.SendEvent(eventNameTryRemoveCard, eventListner);
			if (!stopCalculate) {


				EventSystem.SendEvent(eventNameRemoveCard, eventListner);
			}
			else {
				stopCalculate = false;
			}
		}

		public void Shuffle(GameObject eventListner) {
			EventSystem.SendEvent(eventNameTryShuffle, eventListner);
			if(!stopCalculate) {
				int lastCardIndex = cardList.Count - 1;
				int index = 0;
				AbstractCard card;
				bool repositioned = false;

				while (!repositioned) {
					if (lastCardIndex == 0) {
						repositioned = true;
					}
					card = cardList[0];
					cardList.RemoveAt(0);
					index = Random.Range(0, cardList.Count + 1);
					cardList.Insert(index, card);
					if (index >= lastCardIndex) {
						lastCardIndex--;
					}
				}

				EventSystem.SendEvent(eventNameShuffle, eventListner);
			}
			else {
				stopCalculate = false;
			}
		}

		public int Count() {
			return cardList.Count;
		}

	}


}