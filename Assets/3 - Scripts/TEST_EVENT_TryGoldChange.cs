﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEST_EVENT : MonoBehaviour {
	public PlayerStatBaseDungeon playerDungeonPoiter;
	public bool switchVar = false;

	public void Event_TryGoldChangeDungeon() {
		if(!switchVar) {
			Debug.Log("NO");
			playerDungeonPoiter.gold.StopCalculate();
			switchVar = !switchVar;
		}
	}

	public void Event_GoldChangeDungeon(float value) {
		if (switchVar) {
			Debug.Log("SI - " + value);
			switchVar = !switchVar;
		}
	}

}
